# Varys

## What is varys ?

Varys is a bunch of C++/Qt header files that adds a C++ binding mechanism to Qt and compatible with QML.
Those classes are mostly:

* ```Nullable<T>```
* ```Observable<T>```
* ```Mutable<T>```
* ```Validatable<T>``` (PoC/experiment only)
* ```ObjectList<T>``` (PoC/experiment only)
* ```ObservableList<T>``` (PoC/experiment only)

The idea behind varys is about allowing manipulating data in a more declarative way, like QML does, but with the strength of the C++ language.

If QML is extremely good for the view part of the code, when the codebase grows, the lack of strongly typed and static compiler becomes more and more of an issue.

A good summary of this problem is exposed by Tomas McGuire with his tips 10 to 13 in this [video](https://youtu.be/vzs5VPTf4QQ?t=23m21s).

This doesn't come at no cost. Due to it's technical implementation, the binding mechanism exposed here is obviously heavier than the Q_PROPERTY mechanism from Qt. There is no exact comparison yet, but it might be a blocker for embedded and performance/RAM constrained environments.

Thanks to it's syntax and mechanisms, varys can be really powerful for a formula engine.

Varys is highly dependent on [Verdigris](https://github.com/woboq/verdigris). It also requires C++11 (Verdigris requires C++14) and the QT version I used was Qt 5.8. Varys hasn't been tested with an earlier version of Qt.

## How to use varys?
You just need to include the files in your project. In the classes using varys, just include <Varys.h>.

There is one exception, you will need to add BindingNotifier.h to the list of files that moc will parse and generate. This is due to the fact that this part still relies on qmake and it felt useless to generate a whole llibrary just for this.

## What is the role of varys and its classes ?

There is a main guideline in varys: each type should be exposed to QML as a natively supported type (bool, double, QObject*, QVariant,...). Moreover, it has to work without using private APIs of Qt.

### Nullable

```Nullable<T>``` is a class that allows a base type to be nullable. It is similar to Nullable Types in C#.

##### Example
```cpp
Nullable<bool> isHuman(nullptr);

// all the following lines are correct
if (isHuman == nullptr)
if (isHuman.isNull())
isHuman = true;
if (isHuman) // be careful with this one, this is clearly not recommended...
isHuman = nullptr;
isHuman.nullate();
```

This class is coded in pure C++ and can be used without Qt.

### Observable

```Observable<T>``` is a class that notifies listeners when its internal value changed.

#### Observable Getters and setters ?

Varys objects don't have getters and setters, it's inside the type. To understand it, let's compare a normal Qt class and a varys one:

```cpp
class Person : public QObject {
    Q_OBJECT
public:
    using QObject::QObject;

    QString name() const { return m_name; }
    void setName(const QString & value) {
    	if (m_name == value)
    		return;
		m_name = value;
		emit nameChanged();
	}
	
signals:
	void nameChanged();
		
private:
    QString m_name;
};

class CallerTest : public QObject {
    Q_OBJECT

public:
    CallerTest(QObject* parent = nullptr) : QObject(parent) {
        connect(&m_person, &Person::nameChanged, this, &CallerTest::handleNameChanged);
    }

private:
    Person m_person{this};

private slots:
    void handleNameChanged() { qDebug() << "slot called ! The name is: " << m_person.name(); }
};
```

With varys, it becomes:

```cpp
class Person : public QObject {
    Q_OBJECT
public:
    using QObject::QObject;

    Observable<QString> name;
};

class CallerTest : public QObject {
    Q_OBJECT

public:
    CallerTest(QObject* parent = nullptr) : QObject(parent) {
        connect(&m_person.name, &Observable<QString>::valueChanged, this, &CallerTest::handleNameChanged);
    }

private:
    Person m_person{this};

private slots:
    void handleNameChanged() { qDebug() << "slot called ! The name is: " << m_person.name; }
};
```

The slot can give more information, the previous and new value:

```cpp
void handleNameChanged(const QString& newValue, const QString previousValue) {
	qDebug() << "slot called ! The name was: " << previousValue << " and now it is: " << newValue; 
}
```

OK, but how to expose it to QML? Well, there is a bunch of macros for you:

```cpp
class Person : public QObject {
    Q_OBJECT
    
    V_PROPERTY_RO(Observable, QString, name)
    
public:
    using QObject::QObject;
};
```

As simple as that, and everything else still works the same. No need to register the types, the QML will only see a ```QString```.

Okay, but until now, it's only syntax simplification. How can we go further?

#### Observable Bindings

There is a method on each ```Observable<T>``` to bind to another observable or an ```std::function```.

##### Example

```cpp
int main(int argc, char *argv[]) {
    Observable<int> foo(0);
    Observable<int> bar(1);

    qDebug() << "foo:" << foo << "| bar:" << bar;
    foo.bind(bar);
    qDebug() << "foo:" << foo << "| bar:" << bar;
    bar = 2;
    qDebug() << "foo:" << foo << "| bar:" << bar;
    foo = 3;
    qDebug() << "foo:" << foo << "| bar:" << bar;
    bar.unbind(foo);
    foo = 4;
    qDebug() << "foo:" << foo << "| bar:" << bar;
    bar = 5;
    qDebug() << "foo:" << foo << "| bar:" << bar;
    
    return 0;
}
```

output:

```
foo: 0 | bar: 1
foo: 1 | bar: 1
foo: 2 | bar: 2
foo: 3 | bar: 3
foo: 4 | bar: 3
foo: 4 | bar: 5
```

We can also bind an expression (i.e. a lambda). Imagine you want a convertor between °C and °F:

```cpp
int main(int argc, char *argv[]) {
    Observable<double> celsius(30.0);
    Observable<double> fahrenheit;

    // just for display purposes
    QObject::connect(&fahrenheit, &Observable<double>::valueChanged, [&fahrenheit, &celsius] () {
        qDebug() << "°C:" << celsius << "| °F:" << fahrenheit;
    });

    fahrenheit.bind(
        [&celsius] () -> double {
            return celsius * 1.8 + 32;
        },
        &celsius
    );

    celsius += 2.0;

    return 0;
}
```

output:

```
°C: 30 | °F: 86
°C: 32 | °F: 89.6
```

The main difference with QML is the list of properties you want to be binded to must be given as a second parameter of the ```bind()``` function.


### Mutable

```Mutable<T>```are not exclusively reserved to be used with QML, but this is a really good usage example.

A ```Mutable<T>``` object contains 2 ```Observable<T>``` values: ```value``` and ```mutableValue```.

* ```value``` contains the current value of the object
* ```mutableValue``` contains the temporary/mutating value

When exposed to QML, QML will only see the ```value``` and modifies the ```mutableValue```. It's really useful if you want to make verification/modification in C++, if you want to raise a custom ```signal``` in some cases or if you want to display to QML only serialized values.

##### Example:

MutableDemo.qml

```js
import QtQuick 2.3
import MutableDemo 1.0

Item {
    id: root

    implicitWidth: 100
    implicitHeight: 40

    property MutableDemo context: MutableDemo {}

    TextInput {
        id: input
        anchors.fill: parent
        focus: true

        Binding on text {
            value: context.test
        }
 
        onAccepted: {
            context.test = text
        }
    }
}
```

MutableDemo.h

```cpp
#ifndef MUTABLEDEMO_H
#define MUTABLEDEMO_H

#include <Varys/Varys.h>

class MutableDemo : public QObject {
    Q_OBJECT

    V_PROPERTY_RW(Mutable, QString, test)

public:
    MutableDemo(QObject* parent = nullptr) : QObject(parent) {
        connect(&test.mutableValue, &Observable<QString>::valueChanged, this, &MutableDemo::handleMutableValueChanged);
    }

private slots:
    void handleMutableValueChanged() {
        QString source(test.mutableValue);
        test = source.replace(".", ",");
    }
};

#endif
```

So, if we input "123.4", it will change it into "123,4".

### Validatable
```[PoC/Experimental]```

This type is an Observable with a validation function (the value will only be assigned if the validator function returns true). More to come once stabilized.

### ObjectList
```[PoC/Experimental]```

This list intends to simplify QObject list exposition to QML. It exposes a list of QObject as an AbstractListModel to QML. More to come once stabilized.

### ObservableList
```[PoC/Experimental]```

This list combines an ObjectList and Observable types. The goal is to be able to get notified when a particular value or index changed and update another list accordingly, but with only one signal and one change. More to come once stabilized.

### V_PROPERTY macros

There are 3 main ```V_PROPERTY``` macros:

```cpp
V_PROPERTY_RO(VarysType, ValueType, name)
V_PROPERTY_RW(VarysType, ValueType, name)
V_PROPERTY_LIST(VarysListType, ValueType, name)
```

VarysType can be:

```cpp
Observable
Mutable
Validatable
ObservableNullable
MutableNullable
ValidatableNullable
```

please note that it won't work with any other type, it is due to the way this macro is implemented.

For example:

```cpp
#define V_PROPERTY_RO(VarysType, ValueType, name) \
    V_PROPERTY__RO_##VarysType(ValueType, name)
```

The ```_RO``` or ```_RW``` stands for ReadOnly and ReadWrite for QML access. The C++ always has full access to the property.

The ```V_PROPERTY_LIST``` can take an ```ObjectList``` or ```ObservableList``` as a VarysListType. As this work is still ongoing, this document won't go into details.

Please note that in case you use verdigris for your object and use the ```W_OBJECT``` macro, you will need those variants:

```cpp
WV_PROPERTY_RO(VarysType, ValueType, name)
WV_PROPERTY_RW(VarysType, ValueType, name)
WV_PROPERTY_LIST(VarysListType, ValueType, name)
```

This is due to the fact that the ```W_PROPERTY``` macro is slightly different than the ```Q_PROPERTY``` macro.

## What are the interesting parts of Varys code ?

#### Mutable property macro

Here is the expanded code of a ```Mutable<T>``` property (the Macro parameters are replaced by real values, the macros is expanded for readability and "\\" are removed for the same reason):

```cpp
#define V_PROPERTY_RW(Mutable, double, foo)
    Q_PROPERTY(double foo READ _foo_for_QML WRITE _set_foo_from_QML NOTIFY _foo_valueChanged_QML)
    
    public: 
        Mutable<double> foo{this};
    private: 
        double _foo_for_QML() { 
            QObject::connect(&foo.value, SIGNAL(valueChanged(double, double)), this, SIGNAL(_foo_valueChanged_QML(double, double)), Qt::UniqueConnection); 
            return double(foo.value.value()); 
        } 
        void _set_foo_from_QML(double& valueFromQml) { 
            QObject::connect(&foo.value, SIGNAL(valueChanged(double, double)), this, SIGNAL(_foo_valueChanged_QML(double, double)), Qt::UniqueConnection);
            foo.mutableValue = valueFromQml;
        }
    public:
        Q_SIGNAL void _foo_valueChanged_QML(double, double);
```

As we can see, the setter generated by the macro modifies the mutable value, not the value.

The other interesting part is the fact that we connect the signal of the ```Observable<T>``` to the notify signal inside the getter and setter. Why? Because this macro is called within classes, and we don't have access to the constructor of those classes. So the only place where we know the two signals is inside this macro. This trick needs something really important: ```Qt::UniqueConnection```. This could seem like a useless use of CPU resource, but in our projects, it was negligible.

Finally, the last interesting line is the call to the constructor of ```Mutable<double>```:

```cpp
Mutable<double> foo{this};
```

This call to the constructor is of course limited to C++11. As a signal can give you the sender, by getting the parent of this sender, you get the object that owns the ```Observable<double>```. It is rare, but it can be useful.

#### Nullable property macro

Another interesting part is the ```Nullable<T>``` property macro. The parameters of the macro are changed for the same reason we discussed earlier.

```cpp
#define V_PROPERTY_RW(ObservableNullable, double, foo)
    Q_PROPERTY(QVariant foo READ _foo_for_QML WRITE _set_foo_from_QML NOTIFY _foo_valueChanged_QML)
    
    public:
        ObservableNullable<double> foo{this};
    private:
        QVariant _foo_for_QML() {
            QObject::connect(&foo, SIGNAL(valueChanged(Nullable<double>, Nullable<double>)), this, SIGNAL(_foo_valueChanged_QML(Nullable<double>, Nullable<double>)), Qt::UniqueConnection);
            if (foo.value().isNull())
                return QVariant::fromValue<void*>(nullptr);
            
            return QVariant::fromValue<double>(foo.value());
        }
        void _set_foo_from_QML(QVariant& valueFromQml) {
            QObject::connect(&name, SIGNAL(valueChanged(Nullable<double>, Nullable<double>)), this, SIGNAL(_foo_valueChanged_QML(Nullable<double>, Nullable<double>)), Qt::UniqueConnection);
            if (valueFromQml.canConvert<void*>())
                foo = nullptr;
            else
                foo = valueFromQml.value<Type>();
        }
    public:
        Q_SIGNAL void _foo_valueChanged_QML(Nullable<double>, Nullable<double>); \
    private:
```

There are 5 interesting lines. Let's analyze those.

```cpp
Q_PROPERTY(QVariant foo READ _foo_for_QML WRITE _set_foo_from_QML NOTIFY _foo_valueChanged_QML)
```

As we can see, we expose a ```QVariant``` and not a ```double```. This is because we can't expose a ```Nullable<double>```. If we do that, we need to register the type to QML engine and we need to call a ```foo.value()``` in javascript to get the internal value. We can't expose a ```double```either, as this is a ```NullableDouble```. That's the tricky part: we use a ```QVariant``` and depending on the type inside the value, we get/set what's appropriate:

```cpp
return QVariant::fromValue<void*>(nullptr);
```

If our internal value is a ```nullptr```, we give to QML a ```QVariant``` of pointer type, initialized to ```nullptr```. It allows this type of code in QML/javascript:

```javascript
if (foo == null)
...
```

If we don't have a ```nullptr``` inside foo, we give to QML a QVariant of the internal type, initialized to the internal value. This allows this kind of code in QML/javascript:

```javascript
if (foo == null || foo == 0)
...
```

The setter does the exact opposite thanks to ```QVariant::canConvert()``` method.

## What's the future of Varys ?

* A full documentation of the classes, hierarchies and types.
* A usage example of each feature (even if this readme already shows a big part of it).
* [ongoing] Collections (lists) of Varys types. As we want to expose the source type to QML in order to avoid type registration, it could be nice to add these list types.
* an idea to avoid ```.value()``` calls in cpp as much as possible.