#ifndef VARYS_OBJECTLIST_H
#define VARYS_OBJECTLIST_H

#include <QObject>
#include <QAbstractListModel>
#include <QMetaProperty>
#include <wobjectdefs.h>
#include <wobjectimpl.h>

template<typename T> class ObjectList : public QAbstractListModel {
    W_OBJECT(ObjectList)

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override {
        Q_UNUSED(parent)
        return m_list.size();
    }

    QHash<int, QByteArray> roleNames() const override {
        return m_rolesMap;
    }

    virtual QVariant data(const QModelIndex &index, int role) const override {
        return data(index.row(), role);
    }

    virtual bool setData(
        const QModelIndex &index,
        const QVariant &value,
        int role
    ) override {
        return setData(index.row(), value, role);
    }

public:
    explicit ObjectList(QObject* parent = nullptr)
    :
        QAbstractListModel(parent)
    {
        QMetaObject metaItem(T::staticMetaObject);

        m_rolesMap.insert(Qt::DisplayRole, "modelData");

        const int count = metaItem.propertyCount();
        for (int index = 0; index < count; index++) {
            QMetaProperty metaProp = metaItem.property(index);
            QByteArray propName = QByteArray(metaProp.name());
            m_rolesMap.insert(Qt::UserRole + 1 + index, propName);

            if (metaProp.hasNotifySignal()) {
                m_signalIndexToRole.insert(
                    metaProp.notifySignalIndex(),
                    Qt::UserRole + 1 + index
                );
            }
        }

        static const char * HANDLER = "propertyChangedHandler()";
        m_handler = metaObject()->method(metaObject()->indexOfMethod(HANDLER));

        connect(
            this, &QAbstractListModel::rowsInserted,
            this, &ObjectList<T>::sizeChanged
        );
        connect(
            this, &QAbstractListModel::rowsRemoved,
            this, &ObjectList<T>::sizeChanged
        );
    }

    bool isEmpty() const {
        return m_list.isEmpty();
    }

    int size() const {
        return m_list.size();
    }

    const T* operator[] (int index) const {
        return at(index);
    }

    const T* at(int index) const {
        return m_list[index];
    }

    T* get(int index) const {
        return m_list[index];
    }

    // How could we register arg type for a templated type and for a return
    // value? It would allow us using at() in QML instead of QVariant and
    // itemAt().
    QVariant itemAt(int index) const {
        return data(index, Qt::DisplayRole);
    }

    typename QList<T*>::const_iterator constBegin() const {
        return m_list.constBegin();
    }

    typename QList<T*>::const_iterator constEnd() const {
        return m_list.constEnd();
    }

    typename QList<T*>::const_iterator begin() const {
        return m_list.constBegin();
    }

    typename QList<T*>::const_iterator end() const {
        return m_list.constEnd();
    }

    typename QList<T*>::iterator begin() {
        return m_list.begin();
    }

    typename QList<T*>::iterator end() {
        return m_list.end();
    }

    T* first() const {
        return m_list.first();
    }

    T* last() const {
        return m_list.last();
    }

    const QList<T*>& toList() const {
        return m_list;
    }

    bool contains(T* item) const {
        return m_list.contains(item);
    }

    int indexOf(T* item) const {
        return m_list.indexOf(item);
    }

    virtual QVariant data(int index, const QString& role) const {
        const int roleIndex(m_rolesMap.key(role.toLatin1()));
        return data(index, roleIndex);
    }

    virtual QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (m_rolesMap.contains(role) == false)
            return ret;

        if (index < 0 || index >= m_list.size())
            return ret;

        T* item(m_list.at(index));
        if (item == nullptr)
            return ret;

        if (role == Qt::DisplayRole)
            ret.setValue(item);
        else {
            QMetaProperty prop(
                item->metaObject()->property(role - 1 - Qt::UserRole)
            );

            ret.setValue(prop.read(item));
        }

        return ret;
    }

    virtual bool setData(
        int index,
        const QVariant &value,
        const QString& role
    ) {
        const int roleIndex(m_rolesMap.key(role.toLatin1()));
        return setData(index, value, roleIndex);
    }

    virtual bool setData(int index, const QVariant &value, int role) {
        if (m_rolesMap.contains(role) == false)
            return false;

        if (index < 0 || index > m_list.size())
            return false;

        T* item(m_list.at(index));
        if (item == nullptr || role == Qt::DisplayRole)
            return false;

        QMetaProperty property(
            item->metaObject()->property(role - 1 - Qt::UserRole)
        );

        if (property.isWritable() == false)
            return false;

        return property.write(item, value);
    }

    virtual void append(T* item) {
        if (item->parent() == nullptr)
            item->setParent(this);

        const int index(m_list.size());
        beginInsertRows(QModelIndex(), index, index);
        m_list.append(item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item);
    }

    virtual void append(QList<T*>& items) {
        for (auto item : items)
            append(item);
    }

    virtual void prepend(T* item) {
        if (item->parent() == nullptr)
            item->setParent(this);

        beginInsertRows(QModelIndex(), 0, 0);
        m_list.prepend(item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item);
    }

    virtual void prepend(QList<T*>& items) {
        for (auto item : items)
            prepend(item);
    }

    virtual void insert(int index, T* item) {
        if (item->parent() == nullptr)
            item->setParent(this);

        beginInsertRows(QModelIndex(), index, index);
        m_list.insert(index, item);
        listenItem(item);
        endInsertRows();

        emit itemAdded(item);
    }

    virtual void move(int from, int to) {
        const int lowest  = qMin(from, to);
        const int highest = qMax(from, to);

        beginMoveRows(QModelIndex(), highest, highest, QModelIndex(), lowest);
        m_list.move(from, to);
        endMoveRows();
    }

    virtual void replace(T* oldItem, T* newItem) {
        int index = m_list.indexOf(oldItem);

        if (index < 0 || oldItem == nullptr || newItem == nullptr)
            return;

        removeOne(oldItem);
        insert(index, newItem);

        itemReplaced(newItem, index);
    }

    virtual void removeOne(T* item) {
        if (item == nullptr)
            return;

        int index = m_list.indexOf(item);
        beginRemoveRows(QModelIndex(), index, index);
        m_list.removeOne(item);
        unlistenItem(item);
        endRemoveRows();

        emit itemRemoved(item);

        if (item->parent() == this)
            item->deleteLater();
    }

    virtual void clear() {
        beginRemoveRows(QModelIndex(), 0, m_list.size() - 1);

        for (auto item : m_list)
            unlistenItem(item);

        m_list.clear();
        endRemoveRows();
    }

    virtual void fillFrom(ObjectList<T>& sourceList) {
        clear();

        for (int i(0); i < sourceList.size(); i++)
            append(sourceList.get(i));
    }

    W_INVOKABLE(contains, (T*))

    W_INVOKABLE(indexOf, (T*))

    W_INVOKABLE(itemAt, (int))

    W_INVOKABLE(data, (int, const QString&))

public /* signals */:
    void itemAdded(T* item)
    W_SIGNAL(itemAdded, (T*), item)

    void itemRemoved(T* item)
    W_SIGNAL(itemRemoved, (T*), item)

    void itemReplaced(T* item, int index)
    W_SIGNAL(itemReplaced, (T*, int), item, index)

    void sizeChanged()
    W_SIGNAL(sizeChanged)

    // todo: add notify bindings

public /* properties */:
    W_PROPERTY(bool, isEmpty READ isEmpty NOTIFY sizeChanged)
    W_PROPERTY(int, size READ size NOTIFY sizeChanged)
    W_PROPERTY(int, length READ size NOTIFY sizeChanged)
    W_PROPERTY(int, size READ size NOTIFY sizeChanged)

protected:
    QList<T*> m_list;
    QHash<int, QByteArray> m_rolesMap;
    QMetaMethod m_handler;
    QHash<int, int> m_signalIndexToRole;

    void listenItem(T* item) {
        if (item == nullptr)
            return;

        int count(item->metaObject()->propertyCount());
        for (int i(0); i < count; i++) {
            QMetaProperty prop(item->metaObject()->property(i));
            if (strcmp(prop.name(), "objectName") == 0
                || prop.isReadable() == false
                || prop.hasNotifySignal() == false
            ) {
                continue;
            }

            QMetaMethod notifier = prop.notifySignal();

            connect(
                item, notifier,
                this, m_handler,
                Qt::UniqueConnection
            );
        }
    }

    void unlistenItem(T* item) {
        if (item == nullptr)
            return;

        disconnect(this, nullptr, item, nullptr);
        disconnect(item, nullptr, this, nullptr);
    }

protected /* slots */:
    void propertyChangedHandler() {
        T* item = qobject_cast<T*>(sender());
        int index = indexOf(item);
        int signalIndex = senderSignalIndex();
        QModelIndex rowIndex(
            QAbstractListModel::index(index, 0, QModelIndex())
        );

        emit dataChanged(
            rowIndex,
            rowIndex,
            { m_signalIndexToRole.value(signalIndex) }
        );
    }

    W_SLOT(propertyChangedHandler, W_Access::Protected)
};

W_OBJECT_IMPL(ObjectList<T>, template<typename T>)

#endif
