/***
 * VARYS
 * Copyright (c) Gilles Fernandez, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 ***/

#ifndef VARYS_BINDINGNOTIFIER_H
#define VARYS_BINDINGNOTIFIER_H

#include <QObject>

/**
 * @brief The BindingNotifier class represents a QObject that can notify
 * bindings to update their value.
 *
 * If a custom notifier is needed, you have to inherit this class or declare
 * a notifyBindings() signal.
 */
class BindingNotifier : public QObject {
    Q_OBJECT

public:
    using QObject::QObject;

signals:
    /**
     * @brief when emitted, this signal triggers all the registered bindings
     */
    void notifyBindings();
};

enum Binding {
    OneWayFromSource,
    TwoWays
};

#endif // VARYS_BINDINGNOTIFIER_H
