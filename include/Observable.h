#ifndef VARYS_OBSERVABLE_H
#define VARYS_OBSERVABLE_H

#include <QObject>
#include <QMap>
#include <QDebug>
#include <QVariant>
#include <QJSValue>
#include <QDateTime>

#include <wobjectdefs.h>
#include <wobjectimpl.h>

#include <functional>
#include <cmath>

#include "BindingNotifier.h"
#include "Nullable.h"

template<typename T> class Observable : public BindingNotifier {
    W_OBJECT(Observable)

public:
    explicit Observable(QObject* parent = nullptr) : Observable(parent, T()) {}
    explicit Observable(const T& value) : Observable(nullptr, value) {}
    explicit Observable(QObject* parent, const T& value)
    :
        BindingNotifier(parent),
        m_value(value)
    {
        QObject::connect(
            this, &Observable<T>::valueChanged,
            this, &Observable::notifyBindings
        );
    }

    ~Observable<T>() {
        unbind();
    }

    // The raw value is the value inside the object. This is a shared interface
    // with Nullable and Mutable types.
    const T& rawValue() const {
        return m_value;
    }

    const T& value() const {
        return m_value;
    }

    // see implementation after class declaration
    virtual void setValue(const T &value);

    void freeze() {
        m_frozen = true;
        m_frozenValue = m_value;
    }

    void unfreeze(bool commit = true) {
        m_frozen = false;

        if (commit == false || m_value == m_frozenValue)
            return;
        else {
            emit beforeValueChanged(m_value, m_frozenValue);
            emit valueChanged(m_value, m_frozenValue);
        }
    }

    void bind(
        Observable<T>& source,
        Binding binding = Binding::OneWayFromSource
    ) {
        if (&source == this)
            return;

        unbind();

        setValue(source);
        connect(source);

        if (binding == Binding::TwoWays)
            source.connect(*this);
    }

    void bind(const std::function<T()> &func, QObject* dependency) {
        bind(func, QList<QObject*>( { dependency }));
    }

    void bind(
        const std::function<T()>& func,
        const QList<QObject*>& dependencies
    ) {
        unbind();
        m_func = func;
        m_hasFunc = true;

        for (QObject* dependency : dependencies) {
            if (dependency == this)
                continue;

            m_connections.append(
                QObject::connect(
                    dependency, SIGNAL(notifyBindings()),
                    this, SLOT(callFunc())
                )
            );
        }

        setValue(func());
    }

    void unbind() {
        for (QMetaObject::Connection connection : m_connections)
            QObject::disconnect(connection);

        for (auto source : m_sources.values())
            QObject::disconnect(source);

        m_connections.clear();
        m_sources.clear();

        m_func = std::function<T()>();
        m_hasFunc = false;
    }

    void unbind(Observable<T>& source) {
        if (m_sources.keys().contains(&source)) {
            QObject::disconnect(m_sources[&source]);
            m_sources.remove(&source);
        }

        if (source.m_sources.keys().contains(this))
            source.unbind(*this);
    }

    QJSValue toJSValue() const {
        return QJSValue(m_value);
    }

    void fromJSValue(const QJSValue &value) {
//        qDebug() << "js value?" << value.toString();
        QJSValue toEval(value);

        while (toEval.isCallable())
            toEval = toEval.call();

        static const QString type(valueTypeName());

        // a bit tricky... can we do better?
        if (toEval.toString() == "NaN") {
            setValue(nullptr);
            return;
        }

        // todo: add other conversions
        setValue(toEval.toNumber());
    }

    typedef T ValueType;
    const char* valueTypeName() const {
        return typeid(T).name();
    }

    operator T() const { return value(); }

    Observable<T>& operator=(const T& rhs) {
        setValue(rhs);
        return *this;
    }

    Observable<T>& operator=(Observable<T>& rhs) {
        setValue(rhs);
        return *this;
    }

    Observable<T>& operator+=(const T& rhs) {
        T tmp(value());
        tmp += rhs;
        setValue(tmp);
        return *this;
    }

    Observable<T>& operator-=(const T& rhs) {
        T tmp(value());
        tmp -= rhs;
        setValue(tmp);
        return *this;
    }

    Observable<T>& operator*=(const T& rhs) {
        T tmp(value());
        tmp *= rhs;
        setValue(tmp);
        return *this;
    }

    Observable<T>& operator/=(const T& rhs) {
        T tmp(value());
        tmp /= rhs;
        setValue(tmp);
        return *this;
    }

    Observable<T>& operator++() {
        T tmp(value());
        tmp++;
        setValue(tmp);
        return *this;
    }

    T operator++(int) {
        T tmp(*this);
        operator++();
        return tmp;
    }

    Observable<T>& operator--() {
        T tmp(value());
        tmp--;
        setValue(tmp);
        return *this;
    }

    T operator--(int) {
        T tmp(*this);
        operator--();
        return tmp;
    }

public /* signals */:
    void beforeValueChanged(T newValue, T oldValue)
    W_SIGNAL(beforeValueChanged, (T, T), newValue, oldValue)

    void valueChanged(T newValue, T oldValue)
    W_SIGNAL(valueChanged, (T, T), newValue, oldValue)

protected:
    bool m_frozen{false};

    T m_value;
    T m_frozenValue;

    QMap<Observable<T>*, QMetaObject::Connection> m_sources;
    QList<QMetaObject::Connection> m_connections;
    std::function<T()> m_func;
    bool m_hasFunc{false};

    void connect(Observable<T>& source) {
        m_sources.insert(
            &source,
            QObject::connect(
                &source, &Observable<T>::valueChanged,
                this, &Observable<T>::setValue,
                Qt::UniqueConnection
            )
        );
    }

protected /* slots */:
    void callFunc() {
        setValue(m_func());
    }

    W_SLOT(callFunc, W_Access::Protected)
};

W_OBJECT_IMPL(Observable<T>, template<typename T>)

template<typename T> inline void Observable<T>::setValue(const T& value) {
    const T oldValue = m_value;

    if (m_value == value)
        return;

    if (m_frozen)
        m_value = value;
    else {
        emit beforeValueChanged(value, oldValue);
        m_value = value;
        emit valueChanged(value, oldValue);
    }
}

template<> inline void Observable<double>::setValue(const double& value) {
    const double oldValue = m_value;

    if (m_value == value || (std::isnan(m_value) && std::isnan(value)))
        return;

    if (m_frozen)
        m_value = value;
    else {
        emit beforeValueChanged(value, oldValue);
        m_value = value;
        emit valueChanged(value, oldValue);
    }
}

template<typename T> std::ostream& operator<<(
    std::ostream& os,
    const Observable<T>& obj
) {
    os << obj.value();
    return os;
}

template<typename T> std::istream& operator>>(
    std::istream& is,
    Observable<T>& obj
) {
    T value;
    is >> value;
    obj = value;
    return is;
}

template<typename T> T operator+(Observable<T>& lhs, const T& rhs) {
    T value(lhs.value() + rhs);
    return value;
}

template<typename T> T operator-(Observable<T>& lhs, const T& rhs) {
    T value(lhs.value() - rhs);
    return value;
}

template<typename T> T operator*(Observable<T>& lhs, const T& rhs) {
    T value(lhs.value()* rhs);
    return value;
}

template<typename T> T operator/(Observable<T>& lhs, const T& rhs) {
    T value(lhs.value() / rhs);
    return value;
}

template<typename T> bool operator==(const Observable<T>& lhs, const T& rhs) {
    return lhs.value() == rhs;
}

template<typename T> bool operator==(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return lhs.value() == rhs.value();
}

template<typename T> bool operator!=(const Observable<T>& lhs, const T& rhs) {
    return !(lhs == rhs);
}

template<typename T> bool operator!=(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return !(lhs == rhs);
}

template<typename T> bool operator<=(const Observable<T>& lhs, const T& rhs) {
    return lhs.value() <= rhs;
}

template<typename T> bool operator<=(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return lhs.value() <= rhs.value();
}

template<typename T> bool operator<(const Observable<T>& lhs, const T& rhs) {
    return lhs.value() < rhs;
}

template<typename T> bool operator<(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return lhs.value() < rhs.value();
}

template<typename T> bool operator>=(const Observable<T>& lhs, const T& rhs) {
    return lhs.value() >= rhs;
}

template<typename T> bool operator>=(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return lhs.value() >= rhs.value();
}

template<typename T> bool operator>(const Observable<T>& lhs, const T& rhs) {
    return lhs.value() > rhs;
}

template<typename T> bool operator>(
    const Observable<T>& lhs,
    const Observable<T>& rhs
) {
    return lhs.value() > rhs.value();
}

template<class T>
using ObservableNullable = Observable<Nullable<T>>;

#endif // VARYS_OBSERVABLE_H
