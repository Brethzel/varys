#ifndef VARYS_OBSERVABLELIST_H
#define VARYS_OBSERVABLELIST_H

#include <QObject>
#include <QAbstractListModel>
#include <QMetaProperty>
#include <wobjectdefs.h>
#include <wobjectimpl.h>

#include "Observable.h"

#define V_OBSERVABLELIST_IMPL_BEGIN(ListTypeName, VarysType)                    \
template<typename T> class ListTypeName : public QAbstractListModel {           \
    W_OBJECT(ListTypeName)                                                      \
                                                                                \
    /* QAbstractItemModel interface */                                          \
public:                                                                         \
    int rowCount(const QModelIndex &parent) const override {                    \
        Q_UNUSED(parent);                                                       \
        return m_list.size();                                                   \
    }                                                                           \
                                                                                \
    QHash<int, QByteArray> roleNames() const override {                         \
        static const QHash<int, QByteArray> roleNamesMap {                      \
            { Qt::DisplayRole, "modelData" }                                    \
        };                                                                      \
                                                                                \
        return roleNamesMap;                                                    \
    }                                                                           \
                                                                                \
    virtual QVariant data(const QModelIndex &index, int role) const override {  \
        return data(index.row(), role);                                         \
    }                                                                           \
                                                                                \
    virtual bool setData(                                                       \
        const QModelIndex &index,                                               \
        const QVariant &value,                                                  \
        int role                                                                \
    ) override {                                                                \
        return setData(index.row(), value, role);                               \
    }                                                                           \
                                                                                \
public:                                                                         \
    explicit ListTypeName(QObject* parent = nullptr)                            \
    :                                                                           \
        QAbstractListModel(parent)                                              \
    {}                                                                          \
                                                                                \
    bool isEmpty() const {                                                      \
        return m_list.isEmpty();                                                \
    }                                                                           \
                                                                                \
    int size() const {                                                          \
        return m_list.size();                                                   \
    }                                                                           \
                                                                                \
    const VarysType<T>* operator[] (int index) const {                          \
        return at(index);                                                       \
    }                                                                           \
                                                                                \
    const VarysType<T>* at(int index) const {                                   \
        /*qDebug() << "accessing" << index << m_list[index]->value.value();*/   \
        return m_list[index];                                                   \
    }                                                                           \
                                                                                \
    VarysType<T>* get(int index) const {                                        \
        /*qDebug() << "accessing" << index << m_list[index]->value.value();*/   \
        return m_list[index];                                                   \
    }                                                                           \
                                                                                \
    /* How could we register arg type for a templated type and for a return */  \
    /* value? It would allow us using at() in QML instead of QVariant and */    \
    /* itemAt(). */                                                             \
    QVariant itemAt(int index) const {                                          \
        return data(index, Qt::DisplayRole);                                    \
    }                                                                           \
                                                                                \
    typename QList<VarysType<T>*>::iterator begin() {                           \
        return m_list.begin();                                                  \
    }                                                                           \
                                                                                \
    typename QList<VarysType<T>*>::iterator end() {                             \
        return m_list.end();                                                    \
    }                                                                           \
                                                                                \
    typename QList<VarysType<T>*>::const_iterator constBegin() const {          \
        return m_list.constBegin();                                             \
    }                                                                           \
                                                                                \
    typename QList<VarysType<T>*>::const_iterator constEnd() const {            \
        return m_list.constEnd();                                               \
    }                                                                           \
                                                                                \
    VarysType<T>* first() const {                                               \
        return m_list.first();                                                  \
    }                                                                           \
                                                                                \
    VarysType<T>* last() const {                                                \
        return m_list.last();                                                   \
    }                                                                           \
                                                                                \
    const QList<VarysType<T>*>& toList() const {                                \
        return m_list;                                                          \
    }                                                                           \
                                                                                \
    bool contains(VarysType<T>* item) const {                                   \
        return m_list.contains(item);                                           \
    }                                                                           \
                                                                                \
    bool contains(T value) const {                                              \
        for (auto item : m_list) {                                              \
            if (item->rawValue() == value)                                      \
                return true;                                                    \
        }                                                                       \
                                                                                \
        return false;                                                           \
    }                                                                           \
                                                                                \
    int indexOf(VarysType<T>* item) const {                                     \
        return m_list.indexOf(item);                                            \
    }                                                                           \
                                                                                \
    int indexOf(T value) const {                                                \
        for (int i(0); i < size(); i++) {                                       \
            if (m_list[i]->rawValue() == value)                                 \
                return i;                                                       \
        }                                                                       \
                                                                                \
        return -1;                                                              \
    }                                                                           \
                                                                                \
    virtual QVariant data(int index, const QString& role) const {               \
        Q_UNUSED(role);                                                         \
        return data(index);                                                     \
    }                                                                           \

#define V_OBSERVABLELIST_IMPL_END(ListTypeName, VarysType)                      \
    virtual bool setData(                                                       \
        int index,                                                              \
        const QVariant &value,                                                  \
        const QString& role = ""                                                \
    ) {                                                                         \
        /* this is just to have the same API than other lists. */               \
        Q_UNUSED(role);                                                         \
        return setData(index, value, Qt::DisplayRole);                          \
    }                                                                           \
                                                                                \
    /* only there because of the weird setData API, example: */                 \
    /* https://stackoverflow.com/questions/44777999/manipulate-data-in-a-qabstractlistmodel-from-a-qml-listview */ \
    void setItemAt(int index, const QVariant &value) {                          \
        setData(index, value, "");                                              \
    }                                                                           \
                                                                                \
    void append(VarysType<T>* item) {                                           \
        if (item == nullptr)                                                    \
            return;                                                             \
        /*qDebug() << "appending" << item->value.value(); */                    \
                                                                                \
        const int index(m_list.size());                                         \
        beginInsertRows(QModelIndex(), index, index);                           \
        m_list.append(item);                                                    \
        listenItem(item);                                                       \
        endInsertRows();                                                        \
                                                                                \
        emit itemAdded(item);                                                   \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    void append(T value) {                                                      \
        append(new VarysType<T>(this, value));                                  \
    }                                                                           \
                                                                                \
    void append(const QList<VarysType<T>*>& items) {                            \
        for (auto item : items)                                                 \
            append(item);                                                       \
    }                                                                           \
                                                                                \
    void append(const QList<VarysType<T>>& items) {                             \
        for (auto item : items)                                                 \
            append(item);                                                       \
    }                                                                           \
                                                                                \
    void append(const QList<T>& values) {                                       \
        for (auto value : values)                                               \
            append(value);                                                      \
    }                                                                           \
                                                                                \
    void prepend(VarysType<T>* item) {                                          \
        if (item == nullptr)                                                    \
            return;                                                             \
                                                                                \
        beginInsertRows(QModelIndex(), 0, 0);                                   \
        m_list.prepend(item);                                                   \
        listenItem(item);                                                       \
        endInsertRows();                                                        \
                                                                                \
        emit itemAdded(item);                                                   \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    void prepend(T value) {                                                     \
        prepend(new VarysType<T>(this, value));                                 \
    }                                                                           \
                                                                                \
    void prepend(const QList<VarysType<T>*>& items) {                           \
        for (auto item : items)                                                 \
            prepend(item);                                                      \
    }                                                                           \
                                                                                \
    void prepend(const QList<T>& values) {                                      \
        for (auto value : values)                                               \
            prepend(value);                                                     \
    }                                                                           \
                                                                                \
    void insert(int index, VarysType<T>* item) {                                \
        beginInsertRows(QModelIndex(), index, index);                           \
        m_list.insert(index, item);                                             \
        listenItem(item);                                                       \
        endInsertRows();                                                        \
                                                                                \
        emit itemAdded(item);                                                   \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    void insert(int index, T value) {                                           \
        insert(index, new VarysType<T>(this, value));                           \
    }                                                                           \
                                                                                \
    void insert(int index, const QList<VarysType<T>*>& items) {                 \
        for (int i(0); i < items.size(); i++)                                   \
            insert(index + i, items[i]);                                        \
    }                                                                           \
                                                                                \
    void insert(int index, const QList<T>& values) {                            \
        QList<VarysType<T>*> list;                                              \
                                                                                \
        for (auto value : values)                                               \
            list.append(new VarysType<T>(this, value));                         \
                                                                                \
        insert(index, list);                                                    \
    }                                                                           \
                                                                                \
    void move(int from, int to) {                                               \
        const int lowest  = qMin(from, to);                                     \
        const int highest = qMax(from, to);                                     \
                                                                                \
        beginMoveRows(QModelIndex(), highest, highest, QModelIndex(), lowest);  \
        m_list.move(from, to);                                                  \
        endMoveRows();                                                          \
                                                                                \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    void replace(VarysType<T>* oldItem, VarysType<T>* newItem) {                \
        int index(m_list.indexOf(oldItem));                                     \
                                                                                \
        if (index < 0 || oldItem == nullptr || newItem == nullptr)              \
            return;                                                             \
                                                                                \
        removeOne(oldItem);                                                     \
        insert(index, newItem);                                                 \
                                                                                \
        itemChanged(newItem, index);                                            \
    }                                                                           \
                                                                                \
    void replace(T oldValue, T newValue) {                                      \
        replace(fromValue(oldValue), fromValue(newValue));                      \
    }                                                                           \
                                                                                \
    void removeOne(VarysType<T>* item) {                                        \
        if (item == nullptr)                                                    \
            return;                                                             \
                                                                                \
        int index(m_list.indexOf(item));                                        \
        beginRemoveRows(QModelIndex(), index, index);                           \
        m_list.removeOne(item);                                                 \
        unlistentItem(item);                                                    \
        endRemoveRows();                                                        \
                                                                                \
        emit itemRemoved(item);                                                 \
        emit notifyBindings();                                                  \
                                                                                \
        /* if (item->parent() == this) */                                       \
        /*    item->deleteLater(); */                                           \
    }                                                                           \
                                                                                \
    void removeOne(T value) {                                                   \
        removeOne(fromValue(value));                                            \
    }                                                                           \
                                                                                \
    void clear() {                                                              \
        beginRemoveRows(QModelIndex(), 0, m_list.size() - 1);                   \
                                                                                \
        for (auto item : m_list)                                                \
            unlistenItem(item);                                                 \
                                                                                \
        m_list.clear();                                                         \
        endRemoveRows();                                                        \
                                                                                \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    void bind(ListTypeName<T>& source) {                                        \
                                                                                \
    while (m_list.size() > source.size()) {                                     \
        source.append(new VarysType<T>());                                      \
    }                                                                           \
                                                                                \
    while (m_list.size() < source.size()) {                                     \
        append(new VarysType<T>());                                             \
    }                                                                           \
                                                                                \
        for (int i(0); i < source.size(); i++)                                  \
            m_list.at(i)->bind(*source.get(i));                                 \
    }                                                                           \
                                                                                \
    W_INVOKABLE(contains, (T))                                                  \
                                                                                \
    W_INVOKABLE(indexOf, (T))                                                   \
                                                                                \
    W_INVOKABLE(itemAt, (int))                                                  \
                                                                                \
    W_INVOKABLE(setItemAt, (int, const QVariant&))                              \
                                                                                \
    W_INVOKABLE(data, (int, const QString&))                                    \
                                                                                \
public /* signals */:                                                           \
    void itemAdded(VarysType<T>* item)                                          \
    W_SIGNAL(itemAdded, (VarysType<T>*), item)                                  \
                                                                                \
    void itemRemoved(VarysType<T>* item)                                        \
    W_SIGNAL(itemRemoved, (VarysType<T>*), item)                                \
                                                                                \
    void itemChanged(VarysType<T>* item, int index)                             \
    W_SIGNAL(itemChanged, (VarysType<T>*, int), item, index)                    \
                                                                                \
    void notifyBindings()                                                       \
    W_SIGNAL(notifyBindings)                                                    \
                                                                                \
    void sizeChanged()                                                          \
    W_SIGNAL(sizeChanged)                                                       \
                                                                                \
    /* todo : add properties */                                                 \
                                                                                \
protected:                                                                      \
    QList<VarysType<T>*> m_list;                                                \
                                                                                \
    void listenItem(VarysType<T>* item) {                                       \
        if (item == nullptr)                                                    \
            return;                                                             \
                                                                                \
        connect(                                                                \
            item, &VarysType<T>::valueChanged,                                  \
            this, &ListTypeName<T>::handleValueChanged,                         \
            Qt::UniqueConnection                                                \
        );                                                                      \
    }                                                                           \
                                                                                \
    void unlistenItem(VarysType<T>* item) {                                     \
        if (item == nullptr)                                                    \
            return;                                                             \
                                                                                \
        disconnect(                                                             \
            item, &VarysType<T>::valueChanged,                                  \
            this, &ListTypeName<T>::handleValueChanged                          \
        );                                                                      \
    }                                                                           \
                                                                                \
    VarysType<T>* fromValue(T value) {                                          \
        for (auto item : m_list) {                                              \
            if (item->rawValue() == value) {                                    \
                return item;                                                    \
            }                                                                   \
        }                                                                       \
                                                                                \
        return nullptr;                                                         \
    }                                                                           \
                                                                                \
protected /* slots */:                                                          \
    void handleValueChanged() {                                                 \
        VarysType<T>* item = qobject_cast<VarysType<T>*>(sender());             \
        const int row = m_list.indexOf(item);                                   \
                                                                                \
        if (row < 0)                                                            \
            return;                                                             \
                                                                                \
        QModelIndex index = QAbstractListModel::index(row, 0);                  \
        qDebug() << "test2"; \
        emit itemChanged(item, row);                                            \
        emit dataChanged(index, index);                                         \
        emit notifyBindings();                                                  \
    }                                                                           \
                                                                                \
    W_SLOT(handleValueChanged, W_Access::Protected)                             \
};                                                                              \
                                                                                \
W_OBJECT_IMPL(ListTypeName<T>, template<typename T>)

V_OBSERVABLELIST_IMPL_BEGIN(ObservableList, Observable)
    QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (role != Qt::DisplayRole || index < 0 || index >= size())
            return ret;

        Observable<T>* item(m_list.at(index));

        if (item != nullptr)
            ret.setValue(item->rawValue());

        return ret;
    }

    bool setData(int index, const QVariant& value, int role = Qt::DisplayRole) {
        Q_UNUSED(role);

        if (index < 0 || index >= size())
            return false;

        Observable<T>* item(get(index));

        if (item == nullptr)
            return false;

        item->setValue(value.value<T>());

        return true;
    }
V_OBSERVABLELIST_IMPL_END(ObservableList, Observable)

V_OBSERVABLELIST_IMPL_BEGIN(ObservableNullableList, ObservableNullable)
    QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (role != Qt::DisplayRole || index < 0 || index >= size())
            return ret;

        ObservableNullable<T>* item(m_list.at(index));

        if (item == nullptr)
            return ret;

        if (item->value().isNull())
            return QVariant::fromValue<void*>(nullptr);
        else
            return QVariant::fromValue<T>(item->value());
    }

    bool setData(int index, const QVariant& value, int role = Qt::DisplayRole) {
        Q_UNUSED(role);

        if (index < 0 || index >= size())
            return false;

        ObservableNullable<T>* item(get(index));

        if (item == nullptr)
            return false;

        if (value.canConvert<void*>())
            item->setValue(nullptr);
        else
            item->setValue(value.value<T>());

        return true;
    }
V_OBSERVABLELIST_IMPL_END(ObservableNullableList, ObservableNullable)

V_OBSERVABLELIST_IMPL_BEGIN(MutableList, Mutable)
    QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (role != Qt::DisplayRole || index < 0 || index >= size())
            return ret;

        Mutable<T>* item(m_list.at(index));

        if (item != nullptr)
            ret.setValue(item->value.value());

        return ret;
    }

    bool setData(int index, const QVariant& value, int role = Qt::DisplayRole) {
        Q_UNUSED(role);

        if (index < 0 || index >= size())
            return false;

        Mutable<T>* item(get(index));

        if (item == nullptr)
            return false;

        item->mutableValue.setValue(value.value<T>());

        return true;
    }
V_OBSERVABLELIST_IMPL_END(MutableList, Mutable)

V_OBSERVABLELIST_IMPL_BEGIN(MutableNullableList, MutableNullable)
    QVariant data(int index, int role = Qt::DisplayRole) const {
        QVariant ret;

        if (role != Qt::DisplayRole || index < 0 || index >= size())
            return ret;

        const MutableNullable<T>* item(m_list.at(index));

        if (item == nullptr)
            return ret;

        if (item->value.value().isNull())
            return QVariant::fromValue<void*>(nullptr);
        else
            return QVariant::fromValue<T>(item->value.value());

        return ret;
    }

    bool setData(int index, const QVariant& value, int role = Qt::DisplayRole) {
        Q_UNUSED(role);

        if (index < 0 || index >= size())
            return false;

        MutableNullable<T>* item(get(index));

        if (item == nullptr)
            return false;

        if (value.canConvert<void*>())
            item->mutableValue.setValue(nullptr);
        else
            item->mutableValue.setValue(value.value<T>());

        return true;
    }
V_OBSERVABLELIST_IMPL_END(MutableNullableList, MutableNullable)

// TODO: adding an ObservableListWithQmlAccess?
// itemAt for child?
// W_INVOKABLE clear() for child?
// W_INVOKABLE removeOne() for child?
// W_INVOKABLE replace() for child?
// W_INVOKABLE move() for child?
// W_INVOKABLE insert() for child?
// W_INVOKABLE insert() for child?
// W_INVOKABLE prepend() for child?
// W_INVOKABLE prepend() for child?
// W_INVOKABLE append() for child?
// W_INVOKABLE indexOf() for child?
// W_INVOKABLE contains() for child?

#endif
