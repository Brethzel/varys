/***
 * VARYS
 * Copyright (c) Gilles Fernandez, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 ***/

#ifndef VARYS_NULLABLE_H
#define VARYS_NULLABLE_H

#include <type_traits>
#include <typeinfo>

template<typename T> class Nullable {

    static_assert(
        std::is_default_constructible<T>::value,
        "T must be default-constructible"
    );

public:
    Nullable() : Nullable(nullptr) {}
    Nullable(const T& value) : m_value(value), m_isNull(false) {}
    Nullable(std::nullptr_t) : m_value(T()), m_isNull(true) {}
    Nullable(const Nullable& other)
    :
        m_value(other.m_value),
        m_isNull(other.m_isNull)
    {}

    // The raw value is the value inside the object. This is a shared interface
    // with Observable and Mutable types.
    const T& rawValue() const {
        return m_value;
    }

    const T& value() const {
        return m_value;
    }

    void setValue(const T& value) {
        m_value = value;
        m_isNull = false;
    }

    bool isNull() const {
        return m_isNull;
    }

    void nullate() {
        m_value = T();
        m_isNull = true;
    }

    Nullable<T>& operator =(const T& value) {
        setValue(value);

        return *this;
    }

    Nullable<T>& operator =(std::nullptr_t) {
        nullate();

        return *this;
    }

    bool operator ==(const T& value) const {
        return m_isNull == false && m_value == value;
    }

    bool operator !=(const T& value) const {
        return !(*this == value);
    }

    bool operator ==(std::nullptr_t) const {
        return m_isNull;
    }

    bool operator !=(std::nullptr_t value) const {
        return !(*this == value);
    }

    operator T() const {
        return m_value;
    }

    typedef T ValueType;
    const char* valueTypeName() const {
        return typeid(T).name();
    }

protected:
    T m_value;
    bool m_isNull;
};

template<typename T> bool operator==(
    const Nullable<T>& lhs,
    const Nullable<T>& rhs
) {
    return rhs.isNull() == lhs.isNull() && rhs.value() == lhs.value();
}

template<typename T> bool operator!=(
    const Nullable<T>& lhs,
    const Nullable<T>& rhs
) {
    return ! (lhs == rhs);
}

#endif // VARYS_NULLABLE_H
