#ifndef VARYS_VALIDATABLE_H
#define VARYS_VALIDATABLE_H

#include "Observable.h"

template<typename T> class Validatable : public Observable<T> {
    W_OBJECT(Validatable)

public:
    using Observable<T>::Observable;
    using Observable<T>::operator=;

    bool valid() const {
        return m_valid;
    }

    virtual void setValidator(
        const std::function<bool (const T &)> &func,
        const QList<QObject*>& dependencies
    ) {
        if (m_hasValidator)
            clearValidator();

        m_hasValidator = true;
        m_validator = func;

        for (QObject* dependency : dependencies) {
            if (dependency == this || dependency == nullptr)
                continue;

            m_validatorConnections.append(
                QObject::connect(
                    dependency, SIGNAL(notifyBindings()),
                    this, SLOT(callValidatableFunc())
                )
            );
        }

        setValue(Observable<T>::value());
    }

    virtual void setValidator(
        const std::function<bool (const T &)> &func,
        QObject* dependency
    ) {
        setValidator(func, QList<QObject*>( { dependency }));
    }

    virtual void setValidator(const std::function<bool (const T&)> &func) {
        setValidator(func, nullptr);
    }

    void clearValidator() {
        m_hasValidator = false;

        for (auto connection : m_validatorConnections)
            QObject::disconnect(connection);

        m_validatorConnections.clear();
    }

    bool hasValidator() const {
        return m_hasValidator;
    }

    virtual void setValue(const T& value) override {
        if (value == Observable<T>::m_value)
            return;

        m_lastValue = value;

        if (m_hasValidator && m_validator(value) == false) {
            m_valid = false;
            return;
        }

        m_valid = true;
        Observable<T>::setValue(value);
    }

    typedef T ValueType;
    const char* valueTypeName() const {
        return typeid(T).name();
    }

public /* signals */:
    void validChanged() W_SIGNAL(validChanged)

public /* properties */:
    W_PROPERTY(bool, valid READ valid NOTIFY validChanged)

protected:
    bool m_valid{true};
    bool m_hasValidator{false};
    T m_lastValue;
    std::function<bool (const T&)> m_validator;
    QList<QMetaObject::Connection> m_validatorConnections;

protected /*slots*/:
    void callValidatableFunc() {
        if (Observable<T>::m_hasFunc)
            Observable<T>::callFunc();
        else
            setValue(m_lastValue);
    }
    W_SLOT(callValidatableFunc, W_Access::Protected)
};

W_OBJECT_IMPL(Validatable<T>, template<typename T>)

template<class T>
using ValidatableNullable = Validatable<Nullable<T>>;

#endif // VARYS_PROTECTED_H
