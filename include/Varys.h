#include "Nullable.h"
#include "BindingNotifier.h"
#include "Observable.h"
#include "Validatable.h"
#include "Mutable.h"
#include "ObjectList.h"
#include "ObservableList.h"

#define V_PERSISTENT true
#define V_TRANSIENT false

#define GET_5TH_ARG(arg1, arg2, arg3, arg4, arg5, ...) arg5

#define V_PROPERTY__GETTER(VarysType, ValueType, name)                         \
    ValueType _##name##_for_QML() {                                            \
        QObject::connect(                                                      \
            &name,                                                             \
            SIGNAL(valueChanged(ValueType, ValueType)),                        \
            this,                                                              \
            SIGNAL(_##name##_valueChanged_QML(ValueType, ValueType)),          \
            Qt::UniqueConnection                                               \
        );                                                                     \
                                                                               \
        return ValueType(name.value());                                        \
    }                                                                          \
    private:

#define V_PROPERTY__SETTER(ValueType, name)                                    \
    void _set_##name##_from_QML(ValueType& valueFromQml) {                     \
        QObject::connect(                                                      \
            &name,                                                             \
            SIGNAL(valueChanged(ValueType, ValueType)),                        \
            this,                                                              \
            SIGNAL(_##name##_valueChanged_QML(ValueType, ValueType)),          \
            Qt::UniqueConnection                                               \
        );                                                                     \
                                                                               \
        name.setValue(valueFromQml);                                           \
    }                                                                          \
    private:

#define V_PROPERTY__SIGNAL(ValueType, name)                                    \
    public:                                                                    \
        Q_SIGNAL void _##name##_valueChanged_QML(ValueType, ValueType);        \
    private:

#define V_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                            \
    public:                                                                    \
        Q_SIGNAL void _##name##_mutableValueChanged_QML(ValueType, ValueType); \
    private:

#define WV_PROPERTY__SIGNAL(ValueType, name)                                   \
    public:                                                                    \
        void _##name##_valueChanged_QML(ValueType oldValue, ValueType newValue)\
        W_SIGNAL(                                                              \
            _##name##_valueChanged_QML,                                        \
            (ValueType, ValueType),                                            \
            oldValue,                                                          \
            newValue                                                           \
        )                                                                      \
    private:

#define WV_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                           \
    public:                                                                    \
        void _##name##_mutableValueChanged_QML(                                \
            ValueType oldValue,                                                \
            ValueType newValue                                                 \
        )                                                                      \
        W_SIGNAL(                                                              \
            _##name##_valueChanged_QML,                                        \
            (ValueType, ValueType),                                            \
            oldValue,                                                          \
            newValue                                                           \
        )                                                                      \
    private:

#define V_PROPERTY__IMPL(VarysType, ValueType, name)                           \
    public:                                                                    \
        VarysType<ValueType> name{this};                                       \
    private:                                                                   \
        V_PROPERTY__GETTER(VarysType, ValueType, name)

#define V_PROPERTY__MUTABLE_IMPL(ValueType, name)                              \
    public:                                                                    \
        Mutable<ValueType> name{this};                                         \
                                                                               \
    private:                                                                   \
        ValueType _##name##_for_QML() {                                        \
            QObject::connect(                                                  \
                &name.value, SIGNAL(valueChanged(ValueType, ValueType)),       \
                this, SIGNAL(_##name##_valueChanged_QML(ValueType, ValueType)),\
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            QObject::connect(                                                  \
                &name.mutableValue,                                            \
                SIGNAL(valueChanged(ValueType, ValueType)),                    \
                this,                                                          \
                SIGNAL(_##name##_mutableValueChanged_QML(ValueType, ValueType)),\
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            return ValueType(name.value.value());                              \
        }

#define V_PROPERTY__MUTABLE_SETTER(ValueType, name)                            \
    private:                                                                   \
        void _set_##name##_from_QML(ValueType& valueFromQml) {                 \
            QObject::connect(                                                  \
                &name.value, SIGNAL(valueChanged(ValueType, ValueType)),       \
                this, SIGNAL(_##name##_valueChanged_QML(ValueType, ValueType)),\
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            QObject::connect(                                                  \
                &name.mutableValue,                                            \
                SIGNAL(valueChanged(ValueType, ValueType)),                    \
                this,                                                          \
                SIGNAL(_##name##_mutableValueChanged_QML(ValueType, ValueType)),\
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            name.mutableValue = valueFromQml;                                  \
        }

#define V_PROPERTY__NULLABLE_IMPL(VarysType, ValueType, name)                  \
    public:                                                                    \
        VarysType##Nullable<ValueType> name{this};                             \
                                                                               \
    private:                                                                   \
        QVariant _##name##_for_QML() {                                         \
            QObject::connect(                                                  \
                &name,                                                         \
                SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),\
                this,                                                          \
                SIGNAL(                                                        \
                    _##name##_valueChanged_QML(                                \
                        Nullable<ValueType>,                                   \
                        Nullable<ValueType>                                    \
                    )                                                          \
                ),                                                             \
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            if (name.value().isNull())                                         \
                return QVariant::fromValue<void*>(nullptr);                    \
                                                                               \
            return QVariant::fromValue<ValueType>(name.value());               \
        }                                                                      \
                                                                               \
    private:

#define V_PROPERTY__NULLABLE_SETTER(ValueType, name)                           \
    void _set_##name##_from_QML(QVariant& valueFromQml) {                      \
        QObject::connect(                                                      \
            &name,                                                             \
            SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),    \
            this,                                                              \
            SIGNAL(                                                            \
                _##name##_valueChanged_QML(                                    \
                    Nullable<ValueType>,                                       \
                    Nullable<ValueType>                                        \
                )                                                              \
            ),                                                                 \
            Qt::UniqueConnection                                               \
        );                                                                     \
                                                                               \
        if (valueFromQml.canConvert<void*>())                                  \
            name = nullptr;                                                    \
        else                                                                   \
            name = valueFromQml.value<ValueType>();                            \
    }

#define V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                           \
    public:                                                                    \
        Q_SIGNAL void _##name##_valueChanged_QML(                              \
            Nullable<ValueType>,                                               \
            Nullable<ValueType>                                                \
        );                                                                     \
    private:

#define V_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                    \
    public:                                                                    \
        Q_SIGNAL void _##name##_mutableValueChanged_QML(                       \
            Nullable<ValueType>,                                               \
            Nullable<ValueType>                                                \
        );                                                                     \
    private:

#define WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                          \
    public:                                                                    \
        void _##name##_valueChanged_QML(                                       \
            Nullable<ValueType> oldValue,                                      \
            Nullable<ValueType> newValue                                       \
        )                                                                      \
        W_SIGNAL(                                                              \
            _##name##_valueChanged_QML,                                        \
            (Nullable<ValueType>, Nullable<ValueType>),                        \
            oldValue,                                                          \
            newValue                                                           \
        )                                                                      \
    private:

#define WV_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                   \
    public:                                                                    \
        void _##name##_mutableValueChanged_QML(                                \
            Nullable<ValueType> oldValue,                                      \
            Nullable<ValueType> newValue                                       \
        )                                                                      \
        W_SIGNAL(                                                              \
            _##name##_mutableValueChanged_QML,                                 \
            (Nullable<ValueType>, Nullable<ValueType>),                        \
            oldValue,                                                          \
            newValue                                                           \
        )                                                                      \
    private:

#define V_PROPERTY__MUTABLENULLABLE_IMPL(VarysType, ValueType, name)           \
    public:                                                                    \
        VarysType##Nullable<ValueType> name{this};                             \
                                                                               \
    private:                                                                   \
        QVariant _##name##_for_QML() {                                         \
            QObject::connect(                                                  \
                &name.value,                                                   \
                SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),\
                this,                                                          \
                SIGNAL(                                                        \
                    _##name##_valueChanged_QML(                                \
                        Nullable<ValueType>,                                   \
                        Nullable<ValueType>                                    \
                    )                                                          \
                ),                                                             \
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            QObject::connect(                                                  \
                &name.mutableValue,                                            \
                SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),\
                this,                                                          \
                SIGNAL(                                                        \
                    _##name##_mutableValueChanged_QML(                         \
                        Nullable<ValueType>,                                   \
                        Nullable<ValueType>                                    \
                    )                                                          \
                ),                                                             \
                Qt::UniqueConnection                                           \
            );                                                                 \
                                                                               \
            if (name.value.value().isNull())                                   \
                return QVariant::fromValue<void*>(nullptr);                    \
                                                                               \
            return QVariant::fromValue<ValueType>(name.value.value());         \
        }                                                                      \
                                                                               \
    private:

#define V_PROPERTY__MUTABLENULLABLE_SETTER(ValueType, name)                    \
    void _set_##name##_from_QML(QVariant& valueFromQml) {                      \
        QObject::connect(                                                      \
            &name.value,                                                       \
            SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),    \
            this,                                                              \
            SIGNAL(                                                            \
                _##name##_valueChanged_QML(                                    \
                    Nullable<ValueType>,                                       \
                    Nullable<ValueType>                                        \
                )                                                              \
            ),                                                                 \
            Qt::UniqueConnection                                               \
        );                                                                     \
                                                                               \
        QObject::connect(                                                      \
            &name.mutableValue,                                                \
            SIGNAL(valueChanged(Nullable<ValueType>, Nullable<ValueType>)),    \
            this,                                                              \
            SIGNAL(                                                            \
                _##name##_mutableValueChanged_QML(                             \
                    Nullable<ValueType>,                                       \
                    Nullable<ValueType>                                        \
                )                                                              \
            ),                                                                 \
            Qt::UniqueConnection                                               \
        );                                                                     \
                                                                               \
        if (valueFromQml.canConvert<void*>())                                  \
            name.mutableValue = nullptr;                                       \
        else                                                                   \
            name.mutableValue = valueFromQml.value<ValueType>();               \
    }

#define V_PROPERTY__LIST_IMPL(VarysListType, ValueType, name)                  \
    public:                                                                    \
        VarysListType<ValueType> name{this};                                   \
                                                                               \
    private:                                                                   \
        QAbstractListModel* _##name##_for_QML() {                              \
            return reinterpret_cast<QAbstractListModel *>(&name);              \
        }

#define V_PROPERTY_RO(...)                                                     \
    V_PROPERTY__RO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define V_PROPERTY__RO_CHOOSER(...)                                            \
    GET_5TH_ARG(__VA_ARGS__, V_PROPERTY__RO_4_ARGS, V_PROPERTY__RO_3_ARGS,,, )

#define V_PROPERTY__RO_3_ARGS(VarysType, ValueType, name)                      \
    V_PROPERTY__RO_##VarysType(ValueType, name, V_PERSISTENT)

#define V_PROPERTY__RO_4_ARGS(VarysType, ValueType, name, storage)             \
    V_PROPERTY__RO_##VarysType(ValueType, name, storage)

#define WV_PROPERTY_RO(...)                                                    \
    WV_PROPERTY__RO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define WV_PROPERTY__RO_CHOOSER(...)                                           \
    GET_5TH_ARG(__VA_ARGS__, WV_PROPERTY__RO_4_ARGS, WV_PROPERTY__RO_3_ARGS,,, )

#define WV_PROPERTY__RO_3_ARGS(VarysType, ValueType, name)                     \
    WV_PROPERTY__RO_##VarysType(ValueType, name, V_PERSISTENT)

#define WV_PROPERTY__RO_4_ARGS(VarysType, ValueType, name, storage)            \
    WV_PROPERTY__RO_##VarysType(ValueType, name, storage)

#define V_PROPERTY__RO_Observable(ValueType, name, storage)                    \
    V_PROPERTY__IMPL(Observable, ValueType, name)                              \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )                                                                          \

#define WV_PROPERTY__RO_Observable(ValueType, name, storage)                   \
    V_PROPERTY__IMPL(Observable, ValueType, name)                              \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RO_Validatable(ValueType, name, storage)                   \
    V_PROPERTY__IMPL(Validatable, ValueType, name)                             \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RO_Validatable(ValueType, name, storage)                  \
    V_PROPERTY__IMPL(Validatable, ValueType, name)                             \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RO_Mutable(ValueType, name, storage)                       \
    V_PROPERTY__MUTABLE_IMPL(ValueType, name)                                  \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    V_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                                \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RO_Mutable(ValueType, name, storage)                      \
    V_PROPERTY__MUTABLE_IMPL(ValueType, name)                                  \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    WV_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                               \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RO_ObservableNullable(ValueType, name, storage)            \
    V_PROPERTY__NULLABLE_IMPL(Observable, ValueType, name)                     \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RO_ObservableNullable(ValueType, name, storage)           \
    V_PROPERTY__NULLABLE_IMPL(Observable, ValueType, name)                     \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RO_ValidatableNullable(ValueType, name, storage)           \
    V_PROPERTY__NULLABLE_IMPL(Validatable, ValueType, name)                    \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RO_ValidatableNullable(ValueType, name, storage)          \
    V_PROPERTY__NULLABLE_IMPL(Validatable, ValueType, name)                    \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RO_MutableNullable(ValueType, name, storage)               \
    V_PROPERTY__MUTABLENULLABLE_IMPL(Mutable, ValueType, name)                 \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    V_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                        \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RO_MutableNullable(ValueType, name, storage)              \
    V_PROPERTY__MUTABLENULLABLE_IMPL(Mutable, ValueType, name)                 \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    WV_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                       \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY_RW(...)                                                     \
    V_PROPERTY__RW_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define V_PROPERTY__RW_CHOOSER(...)                                            \
    GET_5TH_ARG(__VA_ARGS__, V_PROPERTY__RW_4_ARGS, V_PROPERTY__RW_3_ARGS,,, )

#define V_PROPERTY__RW_3_ARGS(VarysType, ValueType, name)                      \
    V_PROPERTY__RW_##VarysType(ValueType, name, V_PERSISTENT)

#define V_PROPERTY__RW_4_ARGS(VarysType, ValueType, name, storage)             \
    V_PROPERTY__RW_##VarysType(ValueType, name, storage)

#define WV_PROPERTY_RW(...)                                                    \
    WV_PROPERTY__RW_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define WV_PROPERTY__RW_CHOOSER(...)                                           \
    GET_5TH_ARG(__VA_ARGS__, WV_PROPERTY__RW_4_ARGS, WV_PROPERTY__RW_3_ARGS,,, )

#define WV_PROPERTY__RW_3_ARGS(VarysType, ValueType, name)                     \
    WV_PROPERTY__RW_##VarysType(ValueType, name, V_PERSISTENT)

#define WV_PROPERTY__RW_4_ARGS(VarysType, ValueType, name, storage)            \
    WV_PROPERTY__RW_##VarysType(ValueType, name, storage)

#define V_PROPERTY__RW_Observable(ValueType, name, storage)                    \
    V_PROPERTY__IMPL(Observable, ValueType, name)                              \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    V_PROPERTY__SETTER(ValueType, name)                                        \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RW_Observable(ValueType, name, storage)                   \
    V_PROPERTY__IMPL(Observable, ValueType, name)                              \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    V_PROPERTY__SETTER(ValueType, name)                                        \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RW_Validatable(ValueType, name, storage)                   \
    V_PROPERTY__IMPL(Validatable, ValueType, name)                             \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    V_PROPERTY__SETTER(ValueType, name)                                        \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define WV_PROPERTY__RW_Validatable(ValueType, name, storage)                  \
    V_PROPERTY__IMPL(Validatable, ValueType, name)                             \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    V_PROPERTY__SETTER(ValueType, name)                                        \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RW_Mutable(ValueType, name, storage)                       \
    V_PROPERTY__MUTABLE_IMPL(ValueType, name)                                  \
    V_PROPERTY__SIGNAL(ValueType, name)                                        \
    V_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                                \
    V_PROPERTY__MUTABLE_SETTER(ValueType, name)                                \
    Q_PROPERTY(                                                                \
        ValueType name                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RW_Mutable(ValueType, name, storage)                      \
    V_PROPERTY__MUTABLE_IMPL(ValueType, name)                                  \
    WV_PROPERTY__SIGNAL(ValueType, name)                                       \
    WV_PROPERTY__MUTABLE_SIGNAL(ValueType, name)                               \
    V_PROPERTY__MUTABLE_SETTER(ValueType, name)                                \
    W_PROPERTY(                                                                \
        ValueType, name                                                        \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RW_ObservableNullable(ValueType, name, storage)            \
    V_PROPERTY__NULLABLE_IMPL(Observable, ValueType, name)                     \
    V_PROPERTY__NULLABLE_SETTER(ValueType, name)                               \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RW_ObservableNullable(ValueType, name, storage)           \
    V_PROPERTY__NULLABLE_IMPL(Observable, ValueType, name)                     \
    V_PROPERTY__NULLABLE_SETTER(ValueType, name)                               \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RW_ValidatableNullable(ValueType, name, storage)           \
    V_PROPERTY__NULLABLE_IMPL(Validatable, ValueType, name)                    \
    V_PROPERTY__NULLABLE_SETTER(ValueType, name)                               \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RW_ValidatableNullable(ValueType, name, storage)          \
    V_PROPERTY__NULLABLE_IMPL(Validatable, ValueType, name)                    \
    V_PROPERTY__NULLABLE_SETTER(ValueType, name)                               \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

#define V_PROPERTY__RW_MutableNullable(ValueType, name, storage)               \
    V_PROPERTY__MUTABLENULLABLE_IMPL(Mutable, ValueType, name)                 \
    V_PROPERTY__MUTABLENULLABLE_SETTER(ValueType, name)                        \
    V_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                               \
    V_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                        \
    Q_PROPERTY(                                                                \
        QVariant name                                                          \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
        STORED storage                                                         \
    )

#define WV_PROPERTY__RW_MutableNullable(ValueType, name, storage)              \
    V_PROPERTY__MUTABLENULLABLE_IMPL(Mutable, ValueType, name)                 \
    V_PROPERTY__MUTABLENULLABLE_SETTER(ValueType, name)                        \
    WV_PROPERTY__NULLABLE_SIGNAL(ValueType, name)                              \
    WV_PROPERTY__MUTABLENULLABLE_SIGNAL(ValueType, name)                       \
    W_PROPERTY(                                                                \
        QVariant, name                                                         \
        STORED storage                                                         \
        READ _##name##_for_QML                                                 \
        WRITE _set_##name##_from_QML                                           \
        NOTIFY _##name##_valueChanged_QML                                      \
    )

// todo: add storage for lists?
#define V_PROPERTY_LIST(VarysListType, ValueType, name)                        \
    V_PROPERTY__LIST_IMPL(VarysListType, ValueType, name)                      \
    Q_PROPERTY(QAbstractListModel* name READ _##name##_for_QML CONSTANT)

#define WV_PROPERTY_LIST(VarysListType, ValueType, name)                       \
    V_PROPERTY__LIST_IMPL(VarysListType, ValueType, name)                      \
    W_PROPERTY(QAbstractListModel*, name READ _##name##_for_QML CONSTANT)
