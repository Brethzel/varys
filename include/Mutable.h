#ifndef VARYS_MUTABLE_H
#define VARYS_MUTABLE_H

#include "Validatable.h"

template<typename T> class Mutable : public BindingNotifier {
    W_OBJECT(Mutable)

public:
    explicit Mutable(QObject* parent = nullptr, const T& source = T()) : BindingNotifier(parent) {
        connect(
            &value, &Observable<T>::valueChanged,
            this, &Mutable<T>::valueChanged
        );

        connect(
            &value, &Observable<T>::valueChanged,
            this, &Mutable<T>::handleValueChanged
        );

        connect(
            &value, &Observable<T>::notifyBindings,
            this, &Mutable<T>::notifyBindings
        );

        value = source;
    }

    explicit Mutable(const T& value) : Mutable(nullptr, value) {}

    Validatable<T> value;
    Validatable<T> mutableValue;

    // The raw value is the value inside the object. This is a shared interface
    // with Observable and Nullable types.
    const T& rawValue() const {
        return value.value();
    }

    operator T() const { return value.value(); }

    Mutable<T>& operator=(const Mutable<T>& rhs) {
        value = rhs.value;
        return *this;
    }

    Mutable<T>& operator=(const Observable<T>& rhs) {
        value = rhs;
        return *this;
    }

    Mutable<T>& operator=(const T& rhs) {
        value = rhs;
        return *this;
    }

    bool isMutating() const {
        return value != mutableValue;
    }

    void bind(Mutable<T>& source, Binding binding = Binding::OneWayFromSource) {
        value = source;
        value.bind(source.value, binding);
        mutableValue.bind(source.mutableValue, binding);
    }

    void bind(const std::function<T()> &func, QObject* dependency) {
        value.bind(func, dependency);
        mutableValue.bind(func, dependency);
    }

    void bind(
        const std::function<T()>& func,
        const QList<QObject*>& dependencies
    ) {
        value.bind(func, dependencies);
        mutableValue.bind(func, dependencies);
    }

    void unbind() {
        value.unbind();
        mutableValue.unbind();
    }

    void unbind(Mutable<T>& source) {
        value.unbind(source.value);
        mutableValue.unbind(source.mutableValue);
    }

    typedef T ValueType;
    const char* valueTypeName() const {
        return typeid(T).name();
    }

public /* signals */:
    void valueChanged(T newValue, T oldValue)
    W_SIGNAL(valueChanged, (T, T), newValue, oldValue)

private slots:
    void handleValueChanged() {
        mutableValue = value;
    }
};

W_OBJECT_IMPL(Mutable<T>, template<typename T>)

template<class T>
using MutableNullable = Mutable<Nullable<T>>;

// todo : adding overload operators for standard operations like in Observable

#endif // VARYS_MUTABLE_H
