#ifndef AUTOREFRESHVIEW_H
#define AUTOREFRESHVIEW_H

#include <QFileSystemWatcher>
#include <QQuickView>

class AutoRefreshView : public QQuickView {
public:
    explicit AutoRefreshView(QWindow *parent = 0);
    AutoRefreshView(QQmlEngine* engine, QWindow *parent);
    AutoRefreshView(const QUrl &source, QWindow *parent = 0);

protected:
    QFileSystemWatcher m_watcher;

    void init();
    void refresh();

protected slots:
    void handleFileChanged(const QString& file);
};

#endif
