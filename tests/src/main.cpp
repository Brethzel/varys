#include <QGuiApplication>
#include <QSurfaceFormat>
#include <QtQml>
#include <QtQuickTest/quicktest.h>

#include <config.h>

#include "internal/AutoRefreshView.h"

#include "cpp/TestObservableDouble.h"
#include "cpp/TestObservableString.h"
#include "cpp/TestValidatable.h"
#include "cpp/TestMutable.h"
#include "cpp/TestObjectList.h"
#include "cpp/TestObservableList.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    AutoRefreshView view;
    view.setTitle("tests");
    view.setMinimumSize(QSize(400, 600));
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    QSurfaceFormat format = view.format();
    format.setSamples(16);
    view.setFormat(format);

    qmlRegisterType<TestObservableDouble>("cpptests", 0, 1, "TestObservableDouble");
    qmlRegisterType<TestObservableString>("cpptests", 0, 1, "TestObservableString");

    qmlRegisterType<TestValidatable>("cpptests", 0, 1, "TestValidatable");
    qmlRegisterType<TestMutable>("cpptests", 0, 1, "TestMutable");

    qmlRegisterType<TestObjectList>("cpptests", 0, 1, "TestObjectList");
    qmlRegisterType<TestObservableList>("cpptests", 0, 1, "TestObservableList");

#ifdef QT_DEBUG
    QString path(TESTS_PROJECT_FOLDER);
    path += "/src/main.qml";
    view.setSource(QUrl(path));
#else
    view.setSource(QUrl("qrc:/src/main.qml"));
#endif

    view.show();

    return app.exec();
}
