import QtQuick 2.3
import cpptests 0.1

Item {
    id: root

    TestObservableDouble { id: testObservableDouble }
    TestObservableString { id: testObservableString }
    TestValidatable { id: testValidatable }
    TestMutable {}
    TestObservableList { id: testObservableList }

    Text {
        anchors.centerIn: parent
        color: "red"
//        text: testObservableDouble.fooRead + " " + testObservableDouble.fooWrite
        text: testObservableList.list1.itemAt(0)
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            testObservableDouble.fooWrite = 44
            testObservableString.fooWrite = "written from qml"
            testValidatable.fooWrite = 44
            testObservableDouble.fooRead = 45
            testObservableString.fooRead = "written from qml"
            testValidatable.fooRead = 45
        }
    }
}
