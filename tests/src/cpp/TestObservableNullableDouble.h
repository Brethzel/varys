#ifndef TESTOBSERVABLENULLABLEDOUBLE_H
#define TESTOBSERVABLENULLABLEDOUBLE_H

#include <QObject>
#include <Varys.h>

class TestObservableNullableDouble : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(ObservableNullable, double, value1)
    V_PROPERTY_RW(ObservableNullable, double, value2)

public:
    explicit TestObservableNullableDouble(QObject* parent = nullptr);
};

#endif
