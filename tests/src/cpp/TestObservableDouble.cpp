#include "TestObservableDouble.h"
#include <QDebug>

TestObservableDouble::TestObservableDouble(QObject *parent) : QObject(parent) {
    QTest::qExec(this, 0, nullptr);

    fooRead = 42;
    fooWrite = 43;
}

void TestObservableDouble::init() {
    m_value1 = -1.0;
    m_value2 = 1.0;

    QCOMPARE(m_value1.value(), -1.0);
    QCOMPARE(m_value2.value(), 1.0);
}

void TestObservableDouble::standardOperators() {
    m_value1 = 0.0;
    QCOMPARE(m_value1.value(), 0.0);

    m_value1 = m_value2;
    QCOMPARE(m_value1.value(), 1.0);

    m_value1 = m_value2 + 1;
    QCOMPARE(m_value1.value(), 2.0);

    m_value1 = m_value1 + m_value2;
    QCOMPARE(m_value1.value(), 3.0);

    m_value1 = m_value2;
    QCOMPARE(m_value1, m_value2);
}

void TestObservableDouble::bindings() {
    init();

    m_value1.bind(m_value2);
    QCOMPARE(m_value1.value(), 1.0);

    m_value1.bind(
        [this] () {
            return m_value2 + 1;
        },
        &m_value2
    );
    QCOMPARE(m_value1.value(), 2.0);
    QCOMPARE(m_value2.value(), 1.0);

    m_value2 = 3;
    QCOMPARE(m_value1.value(), 4.0);
    QCOMPARE(m_value2.value(), 3.0);

    m_value1.bind(m_value2, Binding::TwoWays);
    QCOMPARE(m_value1.value(), 3.0);
    QCOMPARE(m_value2.value(), 3.0);

    m_value2 = 4;
    QCOMPARE(m_value1.value(), 4.0);
    QCOMPARE(m_value2.value(), 4.0);

    m_value1 = 5;
    QCOMPARE(m_value1.value(), 5.0);
    QCOMPARE(m_value2.value(), 5.0);

    m_value1.unbind(m_value2);
    m_value1 = 6;
    QCOMPARE(m_value1.value(), 6.0);
    QCOMPARE(m_value2.value(), 5.0);

    m_value2 = 7;
    QCOMPARE(m_value1.value(), 6.0);
    QCOMPARE(m_value2.value(), 7.0);
}
