#ifndef TESTVALIDATABLE_H
#define TESTVALIDATABLE_H

#include <Varys.h>
#include <QtTest/QtTest>

class TestValidatable : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(Validatable, double, fooRead)
    V_PROPERTY_RW(Validatable, double, fooWrite)

public:
    explicit TestValidatable(QObject* parent = nullptr);

private:
    Validatable<double> m_value1;
    Observable<double> m_value2;

private slots:
    void init();
    void standardOperators();
    void bindings();
    void validator();
};

#endif
