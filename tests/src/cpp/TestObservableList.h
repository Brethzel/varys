#ifndef TESTOBSERVABLELIST_H
#define TESTOBSERVABLELIST_H

#include <QObject>
#include <Varys.h>

class TestObservableList : public QObject {
    Q_OBJECT

    V_PROPERTY_LIST(ObservableList, double, list1)

    // TODO: list properties
public:
    explicit TestObservableList(QObject* parent = nullptr);

//    ObservableList<double> list1;
    ObservableNullableList<double> list2;
    MutableList<double> list3;
    MutableNullableList<double> list4;
};

#endif
