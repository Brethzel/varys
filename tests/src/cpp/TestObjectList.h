#ifndef TEST_H
#define TEST_H

#include <QObject>
#include "Item.h"
#include <ObjectList.h>
#include <QTimer>

class TestObjectList : public QObject {
    Q_OBJECT

    Q_PROPERTY(QAbstractListModel* list READ list CONSTANT)

public:
    explicit TestObjectList(QObject* parent = nullptr);

    ObjectList<Item>* list() const;

private:
    ObjectList<Item>* m_list;
    QTimer m_timer;

signals:
    void itemChanged(Item* item);
};

#endif
