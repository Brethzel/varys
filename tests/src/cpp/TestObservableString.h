#ifndef TESTOBSERVABLESTRING_H
#define TESTOBSERVABLESTRING_H

#include <QObject>
#include <Varys.h>
#include <QtTest/QtTest>

class TestObservableString : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(Observable, QString, fooRead)
    V_PROPERTY_RW(Observable, QString, fooWrite)

public:
    explicit TestObservableString(QObject* parent = nullptr);

private:
    Observable<QString> m_value1;
    Observable<QString> m_value2;

private slots:
    void init();
    void standardOperators();
    void bindings();
};

#endif
