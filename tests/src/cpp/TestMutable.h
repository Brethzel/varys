#ifndef TESTMUTABLE_H
#define TESTMUTABLE_H

#include <QObject>
#include <Varys.h>
#include <QtTest/QtTest>

class TestMutable : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(Mutable, double, value1)
    V_PROPERTY_RW(Mutable, double, value2)

public:
    explicit TestMutable(QObject* parent = nullptr);

private slots:
    void init();
    void standardOperators();
    void bindings();
};

#endif
