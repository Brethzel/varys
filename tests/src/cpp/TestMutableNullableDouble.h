#ifndef TESTMUTABLENULLABLEDOUBLE_H
#define TESTMUTABLENULLABLEDOUBLE_H

#include <QObject>
#include <Varys.h>

class TestMutableNullableDouble : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(MutableNullable, double, value1)
    V_PROPERTY_RW(MutableNullable, double, value2)

public:
    explicit TestMutableNullableDouble(QObject* parent = nullptr);
};

#endif
