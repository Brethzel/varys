#include "TestValidatable.h"
#include <QDebug>

TestValidatable::TestValidatable(QObject *parent) : QObject(parent) {
    QTest::qExec(this, 0, nullptr);
}

void TestValidatable::init() {
    m_value1 = -1.0;
    m_value2 = 1.0;

    QCOMPARE(m_value1.value(), -1.0);
    QCOMPARE(m_value2.value(), 1.0);
}

void TestValidatable::standardOperators() {
    m_value1 = 0.0;
    QCOMPARE(m_value1.value(), 0.0);

    m_value1 = m_value2;
    QCOMPARE(m_value1.value(), 1.0);

    m_value1 = m_value2 + 1;
    QCOMPARE(m_value1.value(), 2.0);

    m_value1 = m_value1 + m_value2;
    QCOMPARE(m_value1.value(), 3.0);

    m_value1 = m_value2;
    QCOMPARE(m_value1.value(), m_value2.value());
}

void TestValidatable::bindings() {
    init();

    m_value1.bind(m_value2);
    QCOMPARE(m_value1.value(), 1.0);

    m_value1.bind(
        [this] () {
            return m_value2 + 1;
        },
        &m_value2
    );

    QCOMPARE(m_value1.value(), 2.0);
    QCOMPARE(m_value2.value(), 1.0);

    m_value2 = 3;
    QCOMPARE(m_value1.value(), 4.0);
    QCOMPARE(m_value2.value(), 3.0);

    m_value1.bind(m_value2, Binding::TwoWays);
    QCOMPARE(m_value1.value(), 3.0);
    QCOMPARE(m_value2.value(), 3.0);

    m_value2 = 4;
    QCOMPARE(m_value1.value(), 4.0);
    QCOMPARE(m_value2.value(), 4.0);

    m_value1 = 5;
    QCOMPARE(m_value1.value(), 5.0);
    QCOMPARE(m_value2.value(), 5.0);

    m_value1.unbind(m_value2);
    m_value1 = 6;
    QCOMPARE(m_value1.value(), 6.0);
    QCOMPARE(m_value2.value(), 5.0);

    m_value2 = 7;
    QCOMPARE(m_value1.value(), 6.0);
    QCOMPARE(m_value2.value(), 7.0);
}

void TestValidatable::validator() {
    m_value1 = -1.0;

    QCOMPARE(m_value1.hasValidator(), false);

    m_value1.setValidator(
        [] (const double& value) {
            return value >= 0.0;
        }
    );

    QCOMPARE(m_value1.hasValidator(), true);
    QCOMPARE(m_value1.value(), -1.0);

    m_value1 = 0.0;
    QCOMPARE(m_value1.value(), 0.0);

    m_value1 = -1.0;
    QCOMPARE(m_value1.value(), 0.0);

    m_value1.clearValidator();
    QCOMPARE(m_value1.hasValidator(), false);

    m_value1 = -1.0;
    QCOMPARE(m_value1.value(), -1.0);

    m_value2 = 0;
    m_value1.setValidator(
        [this] (const double& value) {
            return m_value2 > 0 && value > 0;
        },
        &m_value2
    );

    QCOMPARE(m_value1.value(), -1.0);

    m_value1 = 1;
    QCOMPARE(m_value1.value(), -1.0);

    m_value2 = 1;
    QCOMPARE(m_value1.value(), 1.0);
}
