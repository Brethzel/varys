#include "TestMutable.h"
#include <QDebug>

TestMutable::TestMutable(QObject *parent) : QObject(parent) {
    QTest::qExec(this, 0, nullptr);
}

void TestMutable::init() {
    value1 = -1.0;
    value2 = 1.0;

    QCOMPARE(value1.value.value(), -1.0);
    QCOMPARE(value2.value.value(), 1.0);
}

void TestMutable::standardOperators() {
    value1 = 0.0;
    QCOMPARE(value1.value.value(), 0.0);

    value1 = value2;
    QCOMPARE(value1.value.value(), 1.0);

    value1 = value2 + 1;
    QCOMPARE(value1.value.value(), 2.0);

    value1 = value1 + value2;
    QCOMPARE(value1.value.value(), 3.0);

    value1 = value2;
    QCOMPARE(value1.value.value(), value2.value.value());
}

void TestMutable::bindings() {
    init();

    value1.bind(value2);
    QCOMPARE(value1.value.value(), 1.0);

    value1.value.bind(
        [this] () {
            return value2 + 1;
        },
        &value2
    );

    QCOMPARE(value1.value.value(), 2.0);
    QCOMPARE(value2.value.value(), 1.0);

    value2 = 3;
    QCOMPARE(value1.value.value(), 4.0);
    QCOMPARE(value2.value.value(), 3.0);

    value1.value.bind(value2.value, Binding::TwoWays);
    QCOMPARE(value1.value.value(), 3.0);
    QCOMPARE(value2.value.value(), 3.0);

    value2 = 4;
    QCOMPARE(value1.value.value(), 4.0);
    QCOMPARE(value2.value.value(), 4.0);

    value1 = 5;
    QCOMPARE(value1.value.value(), 5.0);
    QCOMPARE(value2.value.value(), 5.0);

    value1.value.unbind(value2.value);
    value1 = 6;
    QCOMPARE(value1.value.value(), 6.0);
    QCOMPARE(value2.value.value(), 5.0);

    value2 = 7;
    QCOMPARE(value1.value.value(), 6.0);
    QCOMPARE(value2.value.value(), 7.0);
}
