#include "TestObservableString.h"

TestObservableString::TestObservableString(QObject *parent) : QObject(parent) {
    QTest::qExec(this, 0, nullptr);
}

void TestObservableString::init() {
    m_value1 = "one";
    m_value2 = "two";

    QCOMPARE(m_value1.value(), QString("one"));
    QCOMPARE(m_value2.value(), QString("two"));
}

void TestObservableString::standardOperators() {
    m_value1 = "";
    QCOMPARE(m_value1.value(), QString(""));

    m_value1 = m_value2;
    QCOMPARE(m_value1.value(), QString("two"));

    m_value1 = m_value2 + "hello";
    QCOMPARE(m_value1.value(), QString("twohello"));

    m_value1 = m_value1 + m_value2;
    QCOMPARE(m_value1.value(), QString("twohellotwo"));

    m_value1 = m_value2;
    QCOMPARE(m_value1, m_value2);
}

void TestObservableString::bindings() {
    init();

    m_value1.bind(m_value2);
    QCOMPARE(m_value1.value(), QString("two"));

    m_value1.bind(
        [this] () {
            return m_value2 + "one";
        },
        &m_value2
    );
    QCOMPARE(m_value1.value(), QString("twoone"));
    QCOMPARE(m_value2.value(), QString("two"));

    m_value2 = "three";
    QCOMPARE(m_value1.value(), QString("threeone"));
    QCOMPARE(m_value2.value(), QString("three"));

    m_value1.bind(m_value2, Binding::TwoWays);
    QCOMPARE(m_value1.value(), QString("three"));
    QCOMPARE(m_value2.value(), QString("three"));

    m_value2 = "four";
    QCOMPARE(m_value1.value(), QString("four"));
    QCOMPARE(m_value2.value(), QString("four"));

    m_value1 = "five";
    QCOMPARE(m_value1.value(), QString("five"));
    QCOMPARE(m_value2.value(), QString("five"));

    m_value1.unbind(m_value2);
    m_value1 = "six";
    QCOMPARE(m_value1.value(), QString("six"));
    QCOMPARE(m_value2.value(), QString("five"));

    m_value2 = "seven";
    QCOMPARE(m_value1.value(), QString("six"));
    QCOMPARE(m_value2.value(), QString("seven"));
}
