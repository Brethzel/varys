#ifndef VALIDATABLENULLABLEDOUBLE_H
#define VALIDATABLENULLABLEDOUBLE_H

#include <QObject>
#include <Varys.h>

class ValidatableNullableDouble : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(ValidatableNullable, double, value1)
    V_PROPERTY_RW(ValidatableNullable, double, value2)

public:
    explicit ValidatableNullableDouble(QObject* parent = nullptr);
};

#endif
