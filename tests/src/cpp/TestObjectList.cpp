#include "TestObjectList.h"
#include <QDebug>

TestObjectList::TestObjectList(QObject *parent)
:
    QObject(parent),
    m_list(new ObjectList<Item>(this)
) {
    for (int i(0); i < 10; i++) {
        Item* item(new Item(m_list));
        item->setName("Item " + QString::number(i));
        item->setFrequency(i * 100);
        m_list->append(item);
    }

    m_timer.setSingleShot(true);
    m_timer.setInterval(3000);

    connect(&m_timer, &QTimer::timeout, [this] () {
        Item* item(new Item(m_list));
        item->setName("appended item");
        item->setFrequency(0);
        m_list->append(item);

        Item* item2(m_list->get(2));
        item2->setName("changed name");
    });

    m_timer.start();
}

ObjectList<Item>* TestObjectList::list() const {
    return m_list;
}
