#ifndef TESTOBSERVABLEDOUBLE_H
#define TESTOBSERVABLEDOUBLE_H

#include <QObject>
#include <Varys.h>
#include <QtTest/QtTest>

class TestObservableDouble : public QObject {
    Q_OBJECT

    V_PROPERTY_RO(Observable, double, fooRead)
    V_PROPERTY_RW(Observable, double, fooWrite)

public:
    explicit TestObservableDouble(QObject* parent = nullptr);

private:
    Observable<double> m_value1;
    Observable<double> m_value2;

private slots:
    void init();
    void standardOperators();
    void bindings();
};

#endif
